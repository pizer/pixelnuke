/* 
 * File:   PxCommand.h
 * Author: marc
 *
 * Created on March 22, 2015, 2:52 PM
 */

#ifndef PXCOMMAND_H
#define	PXCOMMAND_H

class PxCommand {
public:
    PxCommand();
    PxCommand(const PxCommand& orig);
    virtual ~PxCommand();
private:

};

#endif	/* PXCOMMAND_H */

